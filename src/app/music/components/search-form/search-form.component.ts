import { Component, EventEmitter, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-search-form',
  templateUrl: './search-form.component.html',
  styleUrls: ['./search-form.component.scss']
})
export class SearchFormComponent implements OnInit {

  @Output()
  searchEvent = new EventEmitter <string>();
  constructor() { }

  searched: string = '';

  ngOnInit(): void {
  }
  search(){
    
    this.searchEvent.emit(this.searched);
  }

}
