export type Playlist = {
  id: string;
  name: string;
  public: boolean;
  description: string;
};
