import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

import { Playlist } from '../../models/Playlist';

@Component({
  selector: 'app-playlists-editor',
  templateUrl: './playlists-editor.component.html',
  styleUrls: ['./playlists-editor.component.scss']
})
export class PlaylistsEditorComponent implements OnInit {
  @Input ()
  playlist: Playlist = {
    id: '123',
    name: 'Playlist 123',
    public: true,
    description: 'my favourite playlist'
  }

  @Output ()
  cancelClicked = new EventEmitter ();

  @Output()
  saveClicked = new EventEmitter <Playlist>();
  
  draft: Playlist = {...this.playlist}
  constructor() { }

  ngOnInit(): void {
  }

  ngOnChanges() {
    this.draft = {...this.playlist};
  }
  cancelSelected() {
    this.cancelClicked.emit();
  }

  saveSelected() {
    this.saveClicked.emit(this.draft);
  }

}
