import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { Playlist } from '../../models/Playlist';

@Component({
  selector: 'app-playlists-list',
  templateUrl: './playlists-list.component.html',
  styleUrls: ['./playlists-list.component.scss']
})
export class PlaylistsListComponent implements OnInit {

  @Input() playlists: Playlist[] = [];

  @Input()selected?: Playlist;
  @Output() selectedChange= new EventEmitter<Playlist>();
  constructor() { }
  @Output() deleteEmit = new EventEmitter<Playlist>();

  select(playlist: Playlist){
    //this.selected=playlist;
    this.selectedChange.emit(playlist);
  }

  delete(playlist: Playlist){
   // this.
  }

  ngOnInit(): void {}
}
