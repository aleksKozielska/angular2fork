import { Component, Input, OnInit, Output, EventEmitter } from '@angular/core';

import { Playlist } from '../../models/Playlist';

@Component({
  selector: 'app-playlists-details',
  templateUrl: './playlists-details.component.html',
  styleUrls: ['./playlists-details.component.scss']
})
export class PlaylistsDetailsComponent implements OnInit {
  @Input() 
  playlist: Playlist = {
    id:'123',
    name:'Playlist 123',
    public:true,
    description:'my favourite playlist'
  }

  @Output() 
  editClicked = new EventEmitter();


  constructor() { }

  ngOnInit(): void {
  }

  
  editSelected(){
    this.editClicked.emit();
  }
}
