import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PlaylistsViewComponent } from './containers/playlists-view/playlists-view.component';
import { PlaylistsListComponent } from './components/playlists-list/playlists-list.component';
import { PlaylistsDetailsComponent } from './components/playlists-details/playlists-details.component';
import { PlaylistsEditorComponent } from './components/playlists-editor/playlists-editor.component';
import { FormsModule } from '@angular/forms';



@NgModule({
  declarations: [
    PlaylistsViewComponent,
    PlaylistsListComponent,
    PlaylistsDetailsComponent,
    PlaylistsEditorComponent
  ],
  imports: [
    CommonModule,
    FormsModule
  ],
  exports: [
    PlaylistsViewComponent,
  ]
})
export class PlaylistsModule { }
