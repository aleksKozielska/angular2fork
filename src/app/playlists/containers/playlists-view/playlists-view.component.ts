import { Component, OnInit } from '@angular/core';
import { Playlist } from '../../models/Playlist';
import { PlaylistsRoutingModule } from '../../playlists-routing.module';

@Component({
  selector: 'app-playlists-view',
  templateUrl: './playlists-view.component.html',
  styleUrls: ['./playlists-view.component.scss']
})
export class PlaylistsViewComponent implements OnInit {
  mode: 'details' | 'edit' | 'create' = 'details';

  playlists: Playlist[] = [{
    id: '123',
    name: 'Playlist 123',
    public: true,
    description: 'my favourite playlist'
  }, 
  {
    id: '234',
    name: 'Playlist 234',
    public: false,
    description: 'my favourite playlist'
  }, 
  {
    id: '345',
    name: 'Playlist 34',
    public: true,
    description: 'my favourite playlist'
  }]

  constructor() { }
  selected: Playlist = this.playlists[0];

  edit(){
    this.mode="edit";
  }
  cancel(){
    this.mode="details";
  }
  save(draft: Playlist){
    this.playlists = this.playlists.map(function(p){
      return p.id === draft.id ? {...draft} : p;
    })

  }
  ngOnInit(): void {

  }

}
