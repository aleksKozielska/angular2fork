import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-clock',
  templateUrl: './clock.component.html',
  styleUrls: ['./clock.component.scss']
})
export class ClockComponent implements OnInit {

  time?:string;
  constructor() { 
   

  }

  ngOnInit(): void {
    setInterval(()=>{
      this.time = (new Date()).toLocaleTimeString();
    }, 1000)
  }
  

}
