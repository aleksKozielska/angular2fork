import { Injectable } from '@angular/core';
import { Album } from '../../model/album';
import { HttpClient } from '@angular/common/http'
import { Observable } from 'rxjs';
import { AuthService } from '../auth/auth.service';


interface AlbumResponse {
  albums: {
    items: Album[]
  }
}
@Injectable({
  providedIn: 'root'
})
export class AlbumSearchService {

  constructor(
    private http: HttpClient,
    private auth: AuthService) { }

    // 
    // holoyis165@bulkbye.com

  getAlbums(query: string) {
    return this.http
      .get<AlbumResponse>(`https://api.spotify.com/v1/search`, {
        params: { q: query, type: 'album' },
        headers: {
          'Authorization': 'Bearer ' + this.auth.getToken()
        }
      })
  }

}
